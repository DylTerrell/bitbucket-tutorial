from PIL import Image 

img1 = Image.open("Toronto.jpg")
img2 = Image.open("King_Chapel.jpg")

small_img = img2.resize((200,200))

small_img.thumbnail((200,200))

img1_copy = img1.copy()
img1_copy.paste(small_img, (500,50))
img1_copy.save("pasted.jpg")